import java.util.*;

public class Hand {
    private int total;
    private int soft;
    private List<Kaart> hand;
    private int playerNumber;

    public Hand(int playerNumber) {
        this.total = 0;
        this.soft = 0;
        this.hand = new ArrayList<>();
        this.playerNumber = playerNumber;
    }

    public void addCard(Kaart kaart) {
        total += kaart.getValue();
        if (kaart.getRang() == Kaartrang.AAS) {
            soft += 1;
        }
        if (soft > 0) {
            if (total > 21) {
                total -= 10;
                soft -= 1;
            }
        }
        hand.add(kaart);
    }

    public Kaart getCard(int index)
    {
        return hand.get(index);
    }

    public void discardHand() {
        hand.clear();
        total = 0;
        soft = 0;
    }

    public int getNumberOfCards()
    {
        return hand.size();
    }

    public void sort()
    {
        Collections.sort(hand);
    }

    public boolean isEmpty()
    {
        return hand.isEmpty();
    }

    public int findCard(Kaart kaart)
    {
        return hand.indexOf(kaart);
    }

    public int getSoft()
    {
        return soft;
    }

    public int evaluateHand()
    {
        return total;
    }

    public int getPlayerNumber()
    {
        return playerNumber;
    }

    @Override
    public String toString()
    {
        return "Player " + playerNumber + ": " + hand.toString();
    }
}