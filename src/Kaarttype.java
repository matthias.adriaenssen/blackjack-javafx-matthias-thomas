import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class Kaarttype {
    private String naam;
    private String symbool;

    public final static Kaarttype KLAVEREN = new Kaarttype( "Klaveren", "k" );
    public final static Kaarttype RUITEN = new Kaarttype( "Ruiten", "r" );
    public final static Kaarttype HARTEN = new Kaarttype( "Harten", "h" );
    public final static Kaarttype SCHOPPEN = new Kaarttype( "Schoppen", "s" );

    public final static List<Kaarttype> VALUES =
            List.of(KLAVEREN, RUITEN, HARTEN, SCHOPPEN);

    private Kaarttype( String nameValue, String symbolValue ) {
        naam = nameValue;
        symbool = symbolValue;
    }

    public String getNaam() {
        return naam;
    }

    public String getSymbool() {
        return symbool;
    }

    @Override
    public String toString() {
        return naam;
    }
}