

import javafx.scene.image.Image;

import java.awt.*;

public class Kaart implements Comparable<Kaart> {
    private Kaarttype typewaarde;
    private Kaartrang rangwaarde;
    private Image cardImage;

    public Kaart( Kaarttype type, Kaartrang rang, Image cardFace ) {
        cardImage = cardFace;
        typewaarde = type;
        rangwaarde = rang;
    }

    public static String getFilename( Kaarttype type, Kaartrang rang ) {
        return "file:resources/cards/"+rang.getSymbool() + type.getSymbool() + ".png";
    }

    public Kaarttype getType() {
        return typewaarde;
    }

    public Kaartrang getRang() {
        return rangwaarde;
    }

    public int getValue() {
        String rank = rangwaarde.getSymbool();
        try{
            // try to turn it into an integer
            return Integer.parseInt(rank);
        } catch (Exception ex){

            // we failed, so it is a letter
            if(rank.equals("a")){
                // it is an ace
                return 11;
            } else {
                // it is a face card
                return 10;
            }
        }
    }

    public Image getCardImage() {
        return cardImage;
    }

    @Override
    public String toString() {
        return typewaarde.toString() + " " + rangwaarde.toString();
    }

    public String rangToString() {
        return rangwaarde.toString();
    }

    public String typeToString() {
        return typewaarde.toString();
    }


    @Override
    public int compareTo(Kaart other) {
        // Compare the values of two cards
        return this.getValue() - other.getValue();
    }

}



