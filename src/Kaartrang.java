
import java.util.*;

public class Kaartrang {
    private String naam;
    private String symbool;

    public final static Kaartrang AAS = new Kaartrang( "Aas", "a" );
    public final static Kaartrang TWEE = new Kaartrang( "Twee", "2" );
    public final static Kaartrang DRIE = new Kaartrang( "Drie", "3" );
    public final static Kaartrang VIER = new Kaartrang( "Vier", "4" );
    public final static Kaartrang VIJF = new Kaartrang( "Vijf", "5" );
    public final static Kaartrang ZES = new Kaartrang( "Zes", "6" );
    public final static Kaartrang ZEVEN = new Kaartrang( "Zeven", "7" );
    public final static Kaartrang ACHT = new Kaartrang( "Acht", "8" );
    public final static Kaartrang NEGEN = new Kaartrang( "Negen", "9" );
    public final static Kaartrang TIEN = new Kaartrang( "Tien", "t" );
    public final static Kaartrang BOER = new Kaartrang( "Boer", "j" );
    public final static Kaartrang KONIGIN = new Kaartrang( "Konigin", "q" );
    public final static Kaartrang KONING = new Kaartrang( "Koning", "k" );

    public final static List<Kaartrang> VALUES =
            List.of(AAS, TWEE, DRIE, VIER, VIJF, ZES, ZEVEN, ACHT, NEGEN, TIEN, BOER, KONIGIN, KONING);

    private Kaartrang( String naamWaarde, String symboolWaarde ) {
        naam = naamWaarde;
        symbool = symboolWaarde;
    }

    public String getName() {
        return naam;
    }

    @Override
    public String toString() {
        return naam;
    }

    public String getSymbool() {
        return symbool;
    }
}