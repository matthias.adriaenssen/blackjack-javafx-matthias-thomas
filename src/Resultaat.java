public class Resultaat {
    private int aantalGewonnen;
    private int aantalVerloren;

    public Resultaat() {
        this.aantalGewonnen = 0;
        this.aantalVerloren = 0;
    }

    public void win() {
        this.aantalGewonnen++;
    }

    public void verlies() {
        this.aantalVerloren++;
    }

    public int getAantalGewonnen() {
        return this.aantalGewonnen;
    }

    public int getAantalVerloren() {
        return this.aantalVerloren;
    }
}