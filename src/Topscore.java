import java.time.LocalDateTime;

public class Topscore implements Comparable<Topscore>
{
    private String spelernaam; //Dit zou eventueel ook een Speler-object kunnen zijn
    private LocalDateTime eindmomentSpel;
    private int score;

    public String getSpelernaam() {
        return spelernaam;
    }

    public void setSpelernaam(String spelernaam) {
        this.spelernaam = spelernaam;
    }

    public LocalDateTime getEindmomentSpel() {
        return eindmomentSpel;
    }

    public void setEindmomentSpel(LocalDateTime eindmomentSpel) {
        this.eindmomentSpel = eindmomentSpel;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Topscore(String spelernaam, LocalDateTime eindmomentSpel, int score) {
        this.spelernaam = spelernaam;
        this.eindmomentSpel = eindmomentSpel;
        this.score = score;
    }

    @Override
    public String toString() {
        return "HighscoreEntry{" +
                "spelernaam='" + spelernaam + '\'' +
                ", eindmomentSpel=" + eindmomentSpel +
                ", score=" + score +
                '}';
    }

    @Override
    public int compareTo(Topscore o) {
        return  o.getScore() - this.score;
    }
}
