import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class SpelInstellingen {
    private int aantalSpelers;
    private String[] namen;

    public void openInstellingen(Stage stage) {


        // Create the popup window
        Stage popupStage = new Stage();
        popupStage.initModality(Modality.APPLICATION_MODAL);
        popupStage.setTitle("Spel Instellingen");

        // Create the main layout for the popup
        VBox popupLayout = new VBox();
        popupLayout.setAlignment(Pos.CENTER);
        popupLayout.setPadding(new Insets(20));
        popupLayout.setSpacing(20);

        // Create the input fields for the number of players and player names
        GridPane inputFields = new GridPane();
        inputFields.setAlignment(Pos.CENTER);
        inputFields.setHgap(10);
        inputFields.setVgap(10);

        Label lblAantalSpelers = new Label("Aantal spelers:");
        Spinner<Integer> spnAantalSpelers = new Spinner<>(0, 5, 0);

        spnAantalSpelers.setEditable(true);

        // Create an array of player name labels and text fields
        Label[] lblNamen = new Label[5];
        TextField[] txtNamen = new TextField[5];

        // Add the player name input fields to the grid pane based on the selected number of players
        for (int i = 0; i < 5; i++) {
            lblNamen[i] = new Label("Speler " + (i + 1) + " naam:");
            txtNamen[i] = new TextField();
            txtNamen[i].setDisable(true);
            inputFields.addRow(i + 1, lblNamen[i], txtNamen[i]);
        }

        // Update the editable property of the first player name text field
        txtNamen[0].setEditable(spnAantalSpelers.getValue() == 1);

        spnAantalSpelers.valueProperty().addListener((obs, oldValue, newValue) -> {
            int aantalSpelers = newValue;
            namen = new String[aantalSpelers];
            for (int i = 0; i < aantalSpelers; i++) {
                txtNamen[i].setDisable(false);
            }
            for (int i = aantalSpelers; i < 5; i++) {
                txtNamen[i].setDisable(true);
            }
            txtNamen[0].setEditable(aantalSpelers == 1);
        });
        inputFields.addRow(0, lblAantalSpelers, spnAantalSpelers);

        // Create the OK and Cancel buttons
        Button btnOK = new Button("OK");
        btnOK.setOnAction(e -> {
            // Get the values from the input fields and store them in the class fields
            aantalSpelers = spnAantalSpelers.getValue();
            namen = new String[aantalSpelers];
            for (int i = 0; i < aantalSpelers; i++) {
                namen[i] = txtNamen[i].getText();
            }

            // Create the Speeltafel object and open a new stage with the Speeltafel scene
            Speeltafel speeltafel = new Speeltafel(aantalSpelers, namen);
            speeltafel.start(new Stage());

            // Close the popup window
            popupStage.close();

        });

        Button btnCancel = new Button("Cancel");
        btnCancel.setOnAction(e -> {
            // Set the class fields to null to indicate that the user cancelled the input
            aantalSpelers = 0;
            namen = null;

            // Close the popup window
            popupStage.close();
        });

        // Add the input fields and buttons to the popup layout
        popupLayout.getChildren().addAll(inputFields, new Separator(), btnOK, btnCancel);

        // Show the popup window and wait for it to be closed
        Scene popupScene = new Scene(popupLayout);
        popupStage.setScene(popupScene);
        popupStage.showAndWait();
    }

    public int getAantalSpelers() {
        return aantalSpelers;
    }

    public String[] getNamen() {
        return namen;
    }
}