

import java.util.*;
import javafx.scene.image.Image;

public class Sloef {
    private java.util.List sloef;
    private int index;

    public Sloef(){ this(1); }
    public Sloef(int numDecks) {
        sloef = new ArrayList();
        index = 0;

        try{
            for(int i = 0; i<numDecks; i++){
                Iterator typeIterator = Kaarttype.VALUES.iterator();
                while ( typeIterator.hasNext() ) {
                    Kaarttype type = (Kaarttype) typeIterator.next();
                    Iterator rangIterator = Kaartrang.VALUES.iterator();
                    while ( rangIterator.hasNext() ) {
                        Kaartrang rang = (Kaartrang) rangIterator.next();
                        Kaart kaart = new Kaart( type, rang, new Image(Kaart.getFilename( type, rang )) );
                        addCard( kaart );
                    }
                }
            }
            shuffle();
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    public void addCard( Kaart kaart ) {
        sloef.add( kaart );
    }

    public int getSizeOfDeck() {
        return sloef.size();
    }

    public int getNumberOfCardsRemaining() {
        return sloef.size() - index;
    }

    public Kaart dealCard() {
        if ( index >= sloef.size() )
            return null;
        else
            return (Kaart) sloef.get( index++ );
    }

    public void shuffle() {
        Collections.shuffle( sloef );
    }

    public boolean isEmpty() {
        if ( index >= sloef.size() )
            return true;
        else
            return false;
    }

    public void restoreDeck() {
        index = 0;
    }
}
