

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import static javafx.application.Application.launch;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class Speeltafel {
    private final Sloef deck = new Sloef(1);
    private final Hand hand = new Hand(1);
    private int currentPlayer = 1;
    private final Hand dealer = new Hand(0);

    private final int aantalSpelers;

    private final String[] namen;

    private boolean eruit;
    private boolean spelerBeurt;

    FlowPane kaarten = new FlowPane(Orientation.HORIZONTAL);
    FlowPane dealerKaarten = new FlowPane(Orientation.HORIZONTAL);



    Label totaalLabel = new Label();
    Label totaalLabelDealer = new Label();

    Label dealerLbl = new Label("Dealer's Hand");
    Label spelerLbl = new Label("Uw Hand");

    Label status = new Label();
    Image imageback = new Image("Tafel.png");

    Label[] naamLabels;

    public Speeltafel(int aantalSpelers, String[] namen) {
        this.aantalSpelers = aantalSpelers;
        this.namen = namen;
        this.naamLabels = new Label[aantalSpelers];


        // create labels for player names
        for (int i = 0; i < aantalSpelers; i++) {
            Label naamLabel = new Label(namen[i]);
            naamLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));
            naamLabel.setTextFill(Color.web("#FFF"));
            naamLabels[i] = naamLabel;
        }
    }

    public Speeltafel() {
        this(1, new String[]{"Speler 1"});
    }

    public void drawCard(Hand hand, FlowPane pane, Label l){
        try{
            Kaart kaart = deck.dealCard();
            ImageView img = new ImageView(kaart.getCardImage());
            pane.getChildren().add(img);
            hand.addCard(kaart);

            int handTotal = hand.evaluateHand();

            StringBuilder total = new StringBuilder();
            if(hand.getSoft() > 0){
                total.append(handTotal-10).append("/");
            }
            total.append(handTotal);
            l.setText(total.toString());
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    public void newDeck(){
        deck.restoreDeck();
        deck.shuffle();
        System.out.println("We hebben de sloef geschud");
    }

    public void newHand() {
        // check if we should shuffle
        if (deck.getNumberOfCardsRemaining() <= deck.getSizeOfDeck() * 0.2) {
            newDeck();
        }

        // clear everything
        hand.discardHand();
        dealer.discardHand();
        kaarten.getChildren().removeAll(kaarten.getChildren());
        dealerKaarten.getChildren().removeAll(dealerKaarten.getChildren());
        totaalLabel.setText("");
        totaalLabelDealer.setText("");

        eruit = false;
        spelerBeurt = true;

        // Kaarten trekken. De speler krijgt er 2, de dealer 1
        for (int i = 1; i <= aantalSpelers; i++) {
            if (i == currentPlayer) {
                drawCard(hand, kaarten, totaalLabel);
                naamLabels[i-1].setText(namen[i-1] + "'s beurt: ");
            } else {
                naamLabels[i-1].setText(namen[i-1]);
            }
        }

        drawCard(dealer, dealerKaarten, totaalLabelDealer);
        drawCard(hand, kaarten, totaalLabel);

        status.setText(namen[currentPlayer - 1] + "'s beurt");
    }

    private void switchToNextPlayer() {
        currentPlayer = (currentPlayer + 1) % aantalSpelers;
        spelerBeurt = currentPlayer == 1;
        newHand();
        status.setText(namen[currentPlayer] + "'s beurt");
    }

    public void start(Stage spelStage) {
        // add labels for player names
        Pane naamPane = new Pane();
        naamPane.setPrefSize(800, 50);




        double naamLabelWidth = 800.0 / aantalSpelers;
        for (int i = 0; i < aantalSpelers; i++) {
            Label naamLabel = naamLabels[i];
            naamLabel.setLayoutX(i * naamLabelWidth);
            naamLabel.setLayoutY(10);
            naamLabel.setPrefWidth(naamLabelWidth);
            naamLabel.setAlignment(Pos.CENTER);
            naamPane.getChildren().add(naamLabel);
        }

        //Spel een aanpasbaar tabblad geven
        spelStage.setMaximized(true);
        spelStage.setIconified(true);


        // Opmaak van de labels
        totaalLabel.setFont(Font.font("Arial", FontWeight.BOLD, 50));
        totaalLabel.setTextFill(Color.web("#FFF"));

        totaalLabelDealer.setFont(Font.font("Arial", FontWeight.BOLD , 50));
        totaalLabelDealer.setTextFill(Color.web("#FFF"));

        status.setTextFill(Color.web("#FFF"));
        status.setFont(Font.font("Arial",24));

        dealerLbl.setTextFill(Color.web("#FFF"));
        dealerLbl.setFont(Font.font("Arial",FontWeight.BOLD, 40));

        spelerLbl.setText(namen[0] + "'s Hand");
        spelerLbl.setTextFill(Color.web("#FFF"));
        spelerLbl.setFont(Font.font("Arial",FontWeight.BOLD, 40));


        BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, false);
        BackgroundImage backgroundImage = new BackgroundImage(imageback, BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        Background background = new Background(backgroundImage);

        Button drawbtn = new Button();
        drawbtn.setText("Hit");
        drawbtn.setOnAction((e) -> {
            if(spelerBeurt == true && eruit != true){
                drawCard(hand, kaarten, totaalLabel);

                if(hand.evaluateHand() > 21){
                    // you busted
                    System.out.println("Je bent verloren");
                    eruit = true;
                    spelerBeurt = false;
                    status.setText("Je bent verloren");
                    status.setTextFill(Color.web("#FF0000"));
                }
            }
        });

        Button endTurnBtn = new Button("End Turn");
        endTurnBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                switchToNextPlayer();
            }
        });

        Button standbtn = new Button();
        standbtn.setText("Stand");
        standbtn.setOnAction((e) -> {
            if(spelerBeurt == true && eruit != true){
                spelerBeurt = false;
                while(dealer.evaluateHand() < 17){
                    drawCard(dealer, dealerKaarten, totaalLabelDealer); //als de dealer
                }

                int playerTotal = hand.evaluateHand();
                int dealerTotal = dealer.evaluateHand();

                System.out.println("Player Hand: "+hand);
                System.out.println("Dealer Hand: "+dealer);

                if(playerTotal == dealerTotal && dealerTotal <= 21){
                    // tie, push
                    System.out.println("Je hebt gepushed");
                    status.setText("Je hebt gepushed");
                    status.setTextFill(Color.web("#FFA500"));

                } else if(playerTotal < dealerTotal && playerTotal > 21){
                    // you lost
                    System.out.println("Jullie zijn beiden verloren");
                    status.setText("Jullie zijn beiden verloren");
                    status.setTextFill(Color.web("#FF0000"));

                } else if(playerTotal < dealerTotal && dealerTotal <= 21){
                    // you lost
                    System.out.println("Je bent verloren");
                    status.setText("Je bent verloren");
                    status.setTextFill(Color.web("#FF0000"));

                } else if (playerTotal > dealerTotal && playerTotal <=21) {
                    // you won
                    System.out.println("Je hebt gewonnen");
                    status.setText("Je hebt gewonnen");
                    status.setTextFill(Color.web("#32CD32"));

                } else if (playerTotal < dealerTotal && dealerTotal > 21) {
                    // you won
                    System.out.println("Je hebt gewonnen");
                    status.setText("Je hebt gewonnen");
                    status.setTextFill(Color.web("#32CD32"));

                } else if (playerTotal > dealerTotal && dealerTotal <= 21) {
                    // you won
                    System.out.println("Je bent verloren");
                    status.setText("Je bent verloren");
                    status.setTextFill(Color.web("#FF0000"));

                }
            }
        });

        Button newbtn = new Button();
        newbtn.setText("New Hand");
        newbtn.setOnAction((e) -> {
            newHand();
            status.setTextFill(Color.web("#FFFFFF"));
        });

        Button btnAfsluiten = new Button("Afsluiten");

        btnAfsluiten.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                spelStage.close();
            }

        });



        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setPadding(new Insets(11.5, 12.5, 13.5, 14.5));
        grid.setHgap(5.5);
        grid.setVgap(5.5);


        grid.add(dealerKaarten, 0, 0, 3, 1);
        dealerKaarten.setPrefWidth(2000);
        grid.add(dealerLbl, 0, 1);
        grid.add(totaalLabelDealer, 1, 1, 2, 1);

        kaarten.setPrefWidth(2000);


        // padding
        Pane p = new Pane();
        p.setPrefSize(200, 100);
        grid.add(p, 0, 2);

        grid.add(kaarten, 0, 3, 3, 1);
        grid.add(spelerLbl, 0, 4);
        grid.add(totaalLabel, 1, 4, 2, 1);
        grid.add(drawbtn,0,5);
        grid.add(standbtn,1,5);
        grid.add(newbtn, 2, 5);
        grid.add(status, 0, 6, 3, 1);
        grid.add(endTurnBtn, 2 , 4);
        grid.setBackground(background);
        grid.add(btnAfsluiten, 0, 8);
        Scene scene = new Scene(grid, 1600, 900);

        spelStage.setTitle("BlackJack");
        spelStage.setScene(scene);
        spelStage.show();

        newDeck();

    }


}
