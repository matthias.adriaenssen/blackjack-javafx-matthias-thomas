public class Speler {
    private String naam;
    private int punten;
    private Hand hand;

    public Speler(String naam) {
        this.naam = naam;
        this.punten = 0;
        this.hand = new Hand(1);
    }

    public String getNaam() {
        return naam;
    }

    public int getPunten() {
        return punten;
    }

    public void setPunten(int punten) {
        this.punten = punten;
    }

    public Hand getHand() {
        return hand;
    }

    public void setHand(Hand hand) {
        this.hand = hand;
    }
}