
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

import java.awt.*;

public class Blackjack extends Application{

    public static void Blackjack(String[] args) {
        Application.launch(args);
    }


    @Override
    public void start(Stage mijnStage) {

        Speeltafel speeltafel = new Speeltafel();
        Stage spelStage = new Stage();

        //1 of meerdere Nodes aanmaken
        Button btnNieuwspel = new Hoofdmenubutton("Nieuw Spel");
        Button btnTopscoren = new Hoofdmenubutton("Topscoren");
        Button btnSpelregels = new Hoofdmenubutton("Spelregels");
        Button btnAfsluiten = new Hoofdmenubutton("Afsluiten");

        Label lblTitle = new Label("Blackjack");
        lblTitle.setFont(new Font("Monospaced", 36));
        lblTitle.setTextFill(Paint.valueOf("#FFFFFF")); // kleur naar wit


        btnNieuwspel.setPrefSize(400,50);
        btnNieuwspel.setBackground(new Background(new BackgroundFill(Color.WHITE,new CornerRadii(20), new Insets(1))));
        btnNieuwspel.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(20), new BorderWidths(2))));
        btnNieuwspel.setFont(new Font("Monospaced", 24));

        btnTopscoren.setPrefSize(400,50);
        btnTopscoren.setBackground(new Background(new BackgroundFill(Color.WHITE, new CornerRadii(20), new Insets(1))));
        btnTopscoren.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(20), new BorderWidths(2))));
        btnTopscoren.setFont(new Font("Monospaced" , 24));

        btnSpelregels.setPrefSize(400,50);
        btnSpelregels.setBackground(new Background(new BackgroundFill(Color.WHITE, new CornerRadii(20), new Insets(1))));
        btnSpelregels.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(20), new BorderWidths(2))));
        btnSpelregels.setFont(new Font("Monospaced" , 24));

        btnAfsluiten.setPrefSize(400,50);
        btnAfsluiten.setBackground(new Background(new BackgroundFill(Color.WHITE, new CornerRadii(20), new Insets(1))));
        btnAfsluiten.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(20), new BorderWidths(2))));
        btnAfsluiten.setFont(new Font("Monospaced" , 24));

        Image mijnImage = new Image("Blackjack_board.jpg");
        ImageView mijnImageView = new ImageView(mijnImage);

        //Pane aanmaken en nodes daaraan toevoegen
        //Het type van pane bepaalt waar de node op het scherm geplaatst wordt
        VBox mijnVBox = new VBox();
        mijnVBox.getChildren().add(btnNieuwspel);
        mijnVBox.getChildren().add(btnTopscoren);
        mijnVBox.getChildren().add(btnSpelregels);
        mijnVBox.getChildren().add(btnAfsluiten);
        mijnVBox.getChildren().add(0, lblTitle); // add at the beginning of the VBox

        mijnVBox.setBackground(new Background( new BackgroundImage(mijnImage, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(100,100,true,true,true, true))));
        mijnVBox.setAlignment(Pos.CENTER);
        mijnVBox.setSpacing(50);

        //mijnVBox.getChildren().add(txtaSpelregels);
        //mijnVBox.getChildren().add(lblSpelregels);
        //Scene toevoegen en pane daaraan toevoegen
        //Je hebt normaal gezien maar 1 scene
        Scene mijnEersteScene = new Scene(mijnVBox);

        btnTopscoren.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // Nieuwe stage maken voor de topscores
                Stage scoreStage = new Stage();
                scoreStage.setTitle("High Scores");

                // De high score screen UI maken
                VBox scoreVBox = new VBox();
                Label scoreLabel = new Label("High Scores");
                scoreLabel.setFont(new Font("Monospaced", 24));
                TextArea scoreTextArea = new TextArea();
                scoreTextArea.setEditable(false);
                scoreTextArea.setPrefSize(200, 150);
                scoreTextArea.setText("1. Player 1 - 1000 points\n2. Player 2 - 900 points\n3. Player 3 - 800 points");

                scoreVBox.getChildren().addAll(scoreLabel, scoreTextArea);
                scoreVBox.setAlignment(Pos.CENTER);
                scoreVBox.setSpacing(10);

                Scene scoreScene = new Scene(scoreVBox, 300, 200);
                scoreStage.setScene(scoreScene);
                scoreStage.show();
            }
        });


        btnNieuwspel.setOnAction(e -> {
            SpelInstellingen instellingen = new SpelInstellingen();
            instellingen.openInstellingen(mijnStage);

            // Get the values from the SpelInstellingen instance
            int aantalSpelers = instellingen.getAantalSpelers();
            String[] namen = instellingen.getNamen();

        });


            //@Override
            //public void handle(ActionEvent event) {
                //speeltafel.start(new Stage());
            //}



        btnSpelregels.setOnAction(e -> {

        });

        btnAfsluiten.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mijnStage.close();
                spelStage.close();
            }
        });

        //Scene toevoegen aan stage die je via parameter binnenkrijgt
        mijnStage.setScene(mijnEersteScene);
        mijnStage.show();
    }
}