public class Saldo {
    private double geld;

    public Saldo(double geld) {
        this.geld = geld;
    }

    public double getGeld() {
        return geld;
    }

    public void setGeld(double geld) {
        this.geld = geld;
    }

    public void verhoogSaldo(double bedrag) {
        geld += bedrag;
    }

    public void verlaagSaldo(double bedrag) {
        geld -= bedrag;
    }
}